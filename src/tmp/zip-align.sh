#!/sbin/bash

mount -o remount,rw /system;

SYSAPP=/system/app/
SYSPRIVAPP=/system/priv-app/
FRAMEWORK=/system/framework/

if [ ! -f $SYSAPP*.odex ]; then

	for apk in $SYSAPP*.apk $SYSPRIVAPP*.apk $FRAMEWORK*.apk $FRAMEWORK*.jar ; do
		zipalign -c 4 $apk;
		ZIPCHECK=$?;
		if [ $ZIPCHECK -eq 1 ]; then
			echo ZipAligning $apk;
			zipalign -f 4 $apk /cache/$apk;
			if [ -e /cache/$apk ]; then
				cp -f -p /cache/$apk $apk;
				rm /cache/$apk;
			fi;
		fi;
	done;
else
	echo "Not needed...";
fi;

mount -o remount,ro /system;

