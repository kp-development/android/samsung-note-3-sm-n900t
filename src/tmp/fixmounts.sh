#!/sbin/sh

# UnMount Our System, Cache, and Data Partitions
umount /system;
umount /userdata;
umount /cache;

# System
e2fsck -y /dev/block/platform/msm_sdcc.1/by-name/system;
tune2fs -O ^has_journal /dev/block/platform/msm_sdcc.1/by-name/system;

# Cache
e2fsck -y /dev/block/platform/msm_sdcc.1/by-name/cache;
tune2fs -O ^has_journal /dev/block/platform/msm_sdcc.1/by-name/cache;

# Data
e2fsck -y /dev/block/platform/msm_sdcc.1/by-name/userdata;
tune2fs -O ^has_journal /dev/block/platform/msm_sdcc.1/by-name/userdata;


