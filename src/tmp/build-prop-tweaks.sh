#!/system/bin/sh
mount -o remount,rw /system;

BUILDPROP="/system/build.prop";

check_prop(){
	busybox grep -c $1 $BUILDPROP;
	return $?;
}

# Make a backup first
cp $BUILDPROP /system/build.prop.bak;

#Add in our tweaks
busybox echo "" >> $BUILDPROP;
busybox echo "" >> $BUILDPROP;
busybox echo "# Kevp75's Tweaks" >> $BUILDPROP;
busybox echo "" >> $BUILDPROP;
#Check for and remove the line
# Wifi Scan
if [ ! $(check_prop "wifi.supplicant_scan_interval") -eq 0 ]; then
	sed -i '/wifi.supplicant_scan_interval/d' $BUILDPROP;
	busybox echo "wifi.supplicant_scan_interval=300" >> $BUILDPROP;
else
	busybox echo "wifi.supplicant_scan_interval=300" >> $BUILDPROP;
fi;
# Dalvik Tweaks
if [ ! $(check_prop "dalvik.vm.dexopt-flags") -eq 0 ]; then
	sed -i '/dalvik.vm.dexopt-flags/d' $BUILDPROP;
	busybox echo "dalvik.vm.dexopt-flags=m=y,v=n,o=v,u=n" >> $BUILDPROP;
else
	busybox echo "dalvik.vm.dexopt-flags=m=y,v=n,o=v,u=n" >> $BUILDPROP;
fi;
if [ ! $(check_prop "dalvik.vm.execution-mode") -eq 0 ]; then
	sed -i '/dalvik.vm.execution-mode/d' $BUILDPROP;
	busybox echo "dalvik.vm.execution-mode=init:jit" >> $BUILDPROP;
else
	busybox echo "dalvik.vm.execution-mode=init:jit" >> $BUILDPROP;
fi;
if [ ! $(check_prop "dalvik.vm.heapstartsize") -eq 0 ]; then
	sed -i '/dalvik.vm.heapstartsize/d' $BUILDPROP;
	busybox echo "dalvik.vm.heapstartsize=16m" >> $BUILDPROP;
else
	busybox echo "dalvik.vm.heapstartsize=16m" >> $BUILDPROP;
fi;
if [ ! $(check_prop "dalvik.vm.heapgrowthlimit") -eq 0 ]; then
	sed -i '/dalvik.vm.heapgrowthlimit/d' $BUILDPROP;
	busybox echo "dalvik.vm.heapgrowthlimit=128m" >> $BUILDPROP;
else
	busybox echo "dalvik.vm.heapgrowthlimit=128m" >> $BUILDPROP;
fi;
if [ ! $(check_prop "dalvik.vm.heapsize") -eq 0 ]; then
	sed -i '/dalvik.vm.heapsize/d' $BUILDPROP;
	busybox echo "dalvik.vm.heapsize=512m" >> $BUILDPROP;
else
	busybox echo "dalvik.vm.heapsize=512m" >> $BUILDPROP;
fi;
if [ ! $(check_prop "dalvik.vm.heaptargetutilization") -eq 0 ]; then
	sed -i '/dalvik.vm.heaptargetutilization/d' $BUILDPROP;
	busybox echo "dalvik.vm.heaptargetutilization=0.75" >> $BUILDPROP;
else
	busybox echo "dalvik.vm.heaptargetutilization=0.75" >> $BUILDPROP;
fi;
if [ ! $(check_prop "dalvik.vm.heapminfree") -eq 0 ]; then
	sed -i '/dalvik.vm.heapminfree/d' $BUILDPROP;
	busybox echo "dalvik.vm.heapminfree=12m" >> $BUILDPROP;
else
	busybox echo "dalvik.vm.heapminfree=12m" >> $BUILDPROP;
fi;
if [ ! $(check_prop "dalvik.vm.heapmaxfree") -eq 0 ]; then
	sed -i '/dalvik.vm.heapmaxfree/d' $BUILDPROP;
	busybox echo "dalvik.vm.heapmaxfree=32m" >> $BUILDPROP;
else
	busybox echo "dalvik.vm.heapmaxfree=32m" >> $BUILDPROP;
fi;
# Net Buffering
if [ ! $(check_prop "net.tcp.buffersize.default") -eq 0 ]; then
	sed -i '/net.tcp.buffersize.default/d' $BUILDPROP;
	busybox echo "net.tcp.buffersize.default=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
else
	busybox echo "net.tcp.buffersize.default=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.tcp.buffersize.wifi") -eq 0 ]; then
	sed -i '/net.tcp.buffersize.wifi/d' $BUILDPROP;
	busybox echo "net.tcp.buffersize.wifi=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
else
	busybox echo "net.tcp.buffersize.wifi=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.tcp.buffersize.umts") -eq 0 ]; then
	sed -i '/net.tcp.buffersize.umts/d' $BUILDPROP;
	busybox echo "net.tcp.buffersize.umts=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
else
	busybox echo "net.tcp.buffersize.umts=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.tcp.buffersize.gprs") -eq 0 ]; then
	sed -i '/net.tcp.buffersize.gprs/d' $BUILDPROP;
	busybox echo "net.tcp.buffersize.gprs=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
else
	busybox echo "net.tcp.buffersize.gprs=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.tcp.buffersize.edge") -eq 0 ]; then
	sed -i '/net.tcp.buffersize.edge/d' $BUILDPROP;
	busybox echo "net.tcp.buffersize.edge=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
else
	busybox echo "net.tcp.buffersize.edge=4096,87380,256960,4096,16384,256960" >> $BUILDPROP;
fi;
# Software Debugging
if [ ! $(check_prop "debug.sf.hw") -eq 0 ]; then
	sed -i '/debug.sf.hw/d' $BUILDPROP;
	busybox echo "debug.sf.hw=1" >> $BUILDPROP;
else
	busybox echo "debug.sf.hw=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "debug.performance.tuning") -eq 0 ]; then
	sed -i '/debug.performance.tuning/d' $BUILDPROP;
	busybox echo "debug.performance.tuning=1" >> $BUILDPROP;
else
	busybox echo "debug.performance.tuning=1" >> $BUILDPROP;
fi;
# Force Home into mem
if [ ! $(check_prop "ro.HOME_APP_ADJ") -eq 0 ]; then
	sed -i '/ro.HOME_APP_ADJ/d' $BUILDPROP;
	busybox echo "ro.HOME_APP_ADJ=1" >> $BUILDPROP;
else
	busybox echo "ro.HOME_APP_ADJ=1" >> $BUILDPROP;
fi;
# DNS Servers
if [ ! $(check_prop "net.wlan0.dns1") -eq 0 ]; then
	sed -i '/net.wlan0.dns1/d' $BUILDPROP;
	busybox echo "net.wlan0.dns1=8.8.8.8" >> $BUILDPROP;
else
	busybox echo "net.wlan0.dns1=8.8.8.8" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.wlan0.dns2") -eq 0 ]; then
	sed -i '/net.wlan0.dns2/d' $BUILDPROP;
	busybox echo "net.wlan0.dns2=8.8.4.4" >> $BUILDPROP;
else
	busybox echo "net.wlan0.dns2=8.8.4.4" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.pdp0.dns1") -eq 0 ]; then
	sed -i '/net.pdp0.dns1/d' $BUILDPROP;
	busybox echo "net.pdp0.dns1=8.8.8.8" >> $BUILDPROP;
else
	busybox echo "net.pdp0.dns1=8.8.8.8" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.pdp0.dns2") -eq 0 ]; then
	sed -i '/net.pdp0.dns2/d' $BUILDPROP;
	busybox echo "net.pdp0.dns2=8.8.4.4" >> $BUILDPROP;
else
	busybox echo "net.pdp0.dns2=8.8.4.4" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.ppp0.dns1") -eq 0 ]; then
	sed -i '/net.ppp0.dns1/d' $BUILDPROP;
	busybox echo "net.ppp0.dns1=8.8.8.8" >> $BUILDPROP;
else
	busybox echo "net.ppp0.dns1=8.8.8.8" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.ppp0.dns2") -eq 0 ]; then
	sed -i '/net.ppp0.dns2/d' $BUILDPROP;
	busybox echo "net.ppp0.dns2=8.8.4.4" >> $BUILDPROP;
else
	busybox echo "net.ppp0.dns2=8.8.4.4" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.eth0.dns1") -eq 0 ]; then
	sed -i '/net.eth0.dns1/d' $BUILDPROP;
	busybox echo "net.eth0.dns1=8.8.8.8" >> $BUILDPROP;
else
	busybox echo "net.eth0.dns1=8.8.8.8" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.eth0.dns2") -eq 0 ]; then
	sed -i '/net.eth0.dns2/d' $BUILDPROP;
	busybox echo "net.eth0.dns2=8.8.4.4" >> $BUILDPROP;
else
	busybox echo "net.eth0.dns2=8.8.4.4" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.gprs.dns1") -eq 0 ]; then
	sed -i '/net.gprs.dns1/d' $BUILDPROP;
	busybox echo "net.gprs.dns1=8.8.8.8" >> $BUILDPROP;
else
	busybox echo "net.gprs.dns1=8.8.8.8" >> $BUILDPROP;
fi;
if [ ! $(check_prop "net.gprs.dns2") -eq 0 ]; then
	sed -i '/net.gprs.dns2/d' $BUILDPROP;
	busybox echo "net.gprs.dns2=8.8.4.4" >> $BUILDPROP;
else
	busybox echo "net.gprs.dns2=8.8.4.4" >> $BUILDPROP;
fi;
# Telephony delay
if [ ! $(check_prop "ro.telephony.call_ring.delay") -eq 0 ]; then
	sed -i '/ro.telephony.call_ring.delay/d' $BUILDPROP;
	busybox echo "ro.telephony.call_ring.delay=0" >> $BUILDPROP;
else
	busybox echo "ro.telephony.call_ring.delay=0" >> $BUILDPROP;
fi;
#Battery savers, media and memory tweaks
if [ ! $(check_prop "persist.adb.notify") -eq 0 ]; then
	sed -i '/persist.adb.notify/d' $BUILDPROP;
	busybox echo "persist.adb.notify=0" >> $BUILDPROP;
else
	busybox echo "persist.adb.notify=0" >> $BUILDPROP;
fi;
if [ ! $(check_prop "persist.sys.purgeable_assets") -eq 0 ]; then
	sed -i '/persist.sys.purgeable_assets/d' $BUILDPROP;
	busybox echo "persist.sys.purgeable_assets=1" >> $BUILDPROP;
else
	busybox echo "persist.sys.purgeable_assets=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "pm.sleep_mode") -eq 0 ]; then
	sed -i '/pm.sleep_mode/d' $BUILDPROP;
	busybox echo "pm.sleep_mode=1" >> $BUILDPROP;
else
	busybox echo "pm.sleep_mode=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.camera.sound.forced") -eq 0 ]; then
	sed -i '/ro.camera.sound.forced/d' $BUILDPROP;
	busybox echo "ro.camera.sound.forced=0" >> $BUILDPROP;
else
	busybox echo "ro.camera.sound.forced=0" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.config.hw_new_wifitopdp") -eq 0 ]; then
	sed -i '/ro.config.hw_new_wifitopdp/d' $BUILDPROP;
	busybox echo "ro.config.hw_new_wifitopdp=1" >> $BUILDPROP;
else
	busybox echo "ro.config.hw_new_wifitopdp=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.config.nocheckin") -eq 0 ]; then
	sed -i '/ro.config.nocheckin/d' $BUILDPROP;
	busybox echo "ro.config.nocheckin=1" >> $BUILDPROP;
else
	busybox echo "ro.config.nocheckin=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.kernel.android.checkjni") -eq 0 ]; then
	sed -i '/ro.kernel.android.checkjni/d' $BUILDPROP;
	busybox echo "ro.kernel.android.checkjni=0" >> $BUILDPROP;
else
	busybox echo "ro.kernel.android.checkjni=0" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.media.dec.aud.flac.enabled") -eq 0 ]; then
	sed -i '/ro.media.dec.aud.flac.enabled/d' $BUILDPROP;
	busybox echo "ro.media.dec.aud.flac.enabled=1" >> $BUILDPROP;
else
	busybox echo "ro.media.dec.aud.flac.enabled=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.media.dec.aud.wma.enabled") -eq 0 ]; then
	sed -i '/ro.media.dec.aud.wma.enabled/d' $BUILDPROP;
	busybox echo "ro.media.dec.aud.wma.enabled=1" >> $BUILDPROP;
else
	busybox echo "ro.media.dec.aud.wma.enabled=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.media.dec.jpeg.memcap") -eq 0 ]; then
	sed -i '/ro.media.dec.jpeg.memcap/d' $BUILDPROP;
	busybox echo "ro.media.dec.jpeg.memcap=8000000" >> $BUILDPROP;
else
	busybox echo "ro.media.dec.jpeg.memcap=8000000" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.media.dec.vid.avi.enabled") -eq 0 ]; then
	sed -i '/ro.media.dec.vid.avi.enabled/d' $BUILDPROP;
	busybox echo "ro.media.dec.vid.avi.enabled=1" >> $BUILDPROP;
else
	busybox echo "ro.media.dec.vid.avi.enabled=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.media.dec.vid.wmv.enabled") -eq 0 ]; then
	sed -i '/ro.media.dec.vid.wmv.enabled/d' $BUILDPROP;
	busybox echo "ro.media.dec.vid.wmv.enabled=1" >> $BUILDPROP;
else
	busybox echo "ro.media.dec.vid.wmv.enabled=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.media.enc.hprof.vid.bps") -eq 0 ]; then
	sed -i '/ro.media.enc.hprof.vid.bps/d' $BUILDPROP;
	busybox echo "ro.media.enc.hprof.vid.bps=8000000" >> $BUILDPROP;
else
	busybox echo "ro.media.enc.hprof.vid.bps=8000000" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.media.enc.jpeg.quality") -eq 0 ]; then
	sed -i '/ro.media.enc.jpeg.quality/d' $BUILDPROP;
	busybox echo "ro.media.enc.jpeg.quality=100" >> $BUILDPROP;
else
	busybox echo "ro.media.enc.jpeg.quality=100" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.ril.disable.power.collapse") -eq 0 ]; then
	sed -i '/ro.ril.disable.power.collapse/d' $BUILDPROP;
	busybox echo "ro.ril.disable.power.collapse=1" >> $BUILDPROP;
else
	busybox echo "ro.ril.disable.power.collapse=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "ro.ril.enable.amr.wideband") -eq 0 ]; then
	sed -i '/ro.ril.enable.amr.wideband/d' $BUILDPROP;
	busybox echo "ro.ril.enable.amr.wideband=1" >> $BUILDPROP;
else
	busybox echo "ro.ril.enable.amr.wideband=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "video.accelerate.hw") -eq 0 ]; then
	sed -i '/video.accelerate.hw/d' $BUILDPROP;
	busybox echo "video.accelerate.hw=1" >> $BUILDPROP;
else
	busybox echo "video.accelerate.hw=1" >> $BUILDPROP;
fi;
if [ ! $(check_prop "windowsmgr.max_events_per_sec") -eq 0 ]; then
	sed -i '/windowsmgr.max_events_per_sec/d' $BUILDPROP;
	busybox echo "windowsmgr.max_events_per_sec=150" >> $BUILDPROP;
else
	busybox echo "windowsmgr.max_events_per_sec=150" >> $BUILDPROP;
fi;
if [ ! $(check_prop "windowsmgr.support_rotation_270") -eq 0 ]; then
	sed -i '/windowsmgr.support_rotation_270/d' $BUILDPROP;
	busybox echo "windowsmgr.support_rotation_270=true" >> $BUILDPROP;
else
	busybox echo "windowsmgr.support_rotation_270=true" >> $BUILDPROP;
fi;
if [ ! $(check_prop "mot.proximity.delay") -eq 0 ]; then
	sed -i '/mot.proximity.delay/d' $BUILDPROP;
	busybox echo "mot.proximity.delay=0" >> $BUILDPROP;
else
	busybox echo "mot.proximity.delay=0" >> $BUILDPROP;
fi;
if [ ! $(check_prop "touch.pressure.scale") -eq 0 ]; then
	sed -i '/touch.pressure.scale/d' $BUILDPROP;
	busybox echo "touch.pressure.scale=0.001" >> $BUILDPROP;
else
	busybox echo "touch.pressure.scale=0.001" >> $BUILDPROP;
fi;

mount -o remount,ro /system;
