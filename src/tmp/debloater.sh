#!/system/xbin/bash

mount -o remount,rw /system;

check_prop(){
	busybox grep -c $1 $2;
	return $?;
}

# Set the properties locations
tmppage1="/tmp/aroma-data/page1.prop";
tmppage2="/tmp/aroma-data/page2.prop";
tmppage3="/tmp/aroma-data/page3.prop";
tmppage4="/tmp/aroma-data/page4.prop";
tmpgoog="/tmp/aroma-data/google.prop";
tmpvariant="/tmp/aroma-data/variant.prop";
tmpatt="/tmp/aroma-data/att.prop";
tmpint="/tmp/aroma-data/international.prop";
tmpspr="/tmp/aroma-data/sprint.prop";
tmptmo="/tmp/aroma-data/tmobile.prop";
tmpvrz="/tmp/aroma-data/verizon.prop";

# Start Debloating
if [ ! $(check_prop "item.0.1=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/QuickMemoWidget.apk";
	busybox rm -f "/system/app/QuickMemoWidget.odex";
fi;
if [ ! $(check_prop "item.0.2=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/SecQuickMemo.apk";
	busybox rm -f "/system/app/SecQuickMemo.odex";
fi;
if [ ! $(check_prop "item.0.3=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/SecQuickMemoApp.apk";
	busybox rm -f "/system/app/SecQuickMemoApp.odex";
fi;
if [ ! $(check_prop "item.0.4=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/QuickMemoCover.apk";
	busybox rm -f "/system/app/QuickMemoCover.odex";
fi;
if [ ! $(check_prop "item.0.5=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/AccuweatherPhone2013.apk";
	busybox rm -f "/system/app/AccuweatherPhone2013.odex";
	busybox rm -f "/system/app/WeatherWidgetDaemon.apk";
	busybox rm -f "/system/app/WeatherWidgetDaemon.odex";
fi;
if [ ! $(check_prop "item.0.6=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/priv-app/Hearingdro.apk";
	busybox rm -f "/system/priv-app/Hearingdro.odex";
fi;
if [ ! $(check_prop "item.0.7=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/AlarmWidget.apk";
	busybox rm -f "/system/app/AlarmWidget.odex";
fi;
if [ ! $(check_prop "item.0.8=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/priv-app/FWUpgrade.apk";
	busybox rm -f "/system/priv-app/FWUpgrade.odex";
	busybox rm -f "/data/app/com.sec.android.fwupgrade-1.apk";
	busybox rm -f "/data/app/com.sec.android.fwupgrade-1.odex";
fi;
if [ ! $(check_prop "item.0.9=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/AllshareControlShare.apk";
	busybox rm -f "/system/app/AllshareControlShare.odex";
fi;
if [ ! $(check_prop "item.0.10=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/AllshareFileShare.apk";
	busybox rm -f "/system/app/AllshareFileShare.odex";
	busybox rm -f "/system/lib/libasf_fileshareserver.so";
	busybox rm -f "/system/lib/libasf_fileshare.so";
fi;
if [ ! $(check_prop "item.0.11=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/AntHalService.apk";
	busybox rm -f "/system/app/AntHalService.odex";
fi;
if [ ! $(check_prop "item.0.12=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/ANTRadioService.apk";
	busybox rm -f "/system/app/ANTRadioService.odex";
fi;
if [ ! $(check_prop "item.0.13=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/ANTPlusPlugins.apk";
	busybox rm -f "/system/app/ANTPlusPlugins.odex";
fi;
if [ ! $(check_prop "item.0.14=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/priv-app/sCloudBackupApp.apk";
	busybox rm -f "/system/priv-app/sCloudBackupApp.odex";
fi;
if [ ! $(check_prop "item.0.15=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/com.mobeam.barcodeService-1.apk.apk";
	busybox rm -f "/system/app/com.mobeam.barcodeService-1.apk.odex";
	busybox rm -f "/data/app/com.mobeam.beepngo-1.apk";
	busybox rm -f "/data/app/com.mobeam.beepngo-1.odex";
fi;
if [ ! $(check_prop "item.0.16=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/Bloomberg.apk";
	busybox rm -f "/system/app/Bloomberg.odex";
fi;
if [ ! $(check_prop "item.0.17=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/Blurb.apk";
	busybox rm -f "/system/app/Blurb.odex";
fi;
if [ ! $(check_prop "item.0.18=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/ChatON_MARKET.apk";
	busybox rm -f "/system/app/ChatON_MARKET.odex";
	busybox rm -f "/system/lib/libChatOnAMSImageFilterLibs-1.0.2.so";
fi;
if [ ! $(check_prop "item.0.19=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/ChocoEUKor.apk";
	busybox rm -f "/system/app/ChocoEUKor.odex";
fi;
if [ ! $(check_prop "item.0.20=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/priv-app/CloudAgent.apk";
	busybox rm -f "/system/priv-app/CloudAgent.odex";
fi;
if [ ! $(check_prop "item.0.21=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/CoolEUKor.apk";
	busybox rm -f "/system/app/CoolEUKor.odex";
fi;
if [ ! $(check_prop "item.0.22=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/DigitalClock.apk";
	busybox rm -f "/system/app/DigitalClock.odex";
fi;
if [ ! $(check_prop "item.0.23=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/DirectConnect.apk";
	busybox rm -f "/system/app/DirectConnect.odex";
fi;
if [ ! $(check_prop "item.0.24=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/priv-app/DirectShareManager.apk";
	busybox rm -f "/system/priv-app/DirectShareManager.odex";
fi;
if [ ! $(check_prop "item.0.25=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/Dropbox.apk";
	busybox rm -f "/system/app/Dropbox.odex";
	busybox rm -f "/system/priv-app/DropboxOOBE.apk";
	busybox rm -f "/system/priv-app/DropboxOOBE.odex";
fi;
if [ ! $(check_prop "item.0.26=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/DualClockDigital.apk";
	busybox rm -f "/system/app/DualClockDigital.odex";
fi;
if [ ! $(check_prop "item.0.27=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/ChartBuilder.apk";
	busybox rm -f "/system/app/ChartBuilder.odex";
fi;
if [ ! $(check_prop "item.0.28=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/MobilePrintSvc_Epson.apk";
	busybox rm -f "/system/app/MobilePrintSvc_Epson.odex";
fi;
if [ ! $(check_prop "item.0.29=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/Evernote_H.apk";
	busybox rm -f "/system/app/Evernote_H.odex";
fi;
if [ ! $(check_prop "item.0.30=1" $tmppage1) -eq 0 ]; then
	busybox rm -f "/system/app/EasyFavoritesContactsWidget.apk";
	busybox rm -f "/system/app/EasyFavoritesContactsWidget.odex";
fi;
if [ ! $(check_prop "item.0.1=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/Flipboard.apk";
	busybox rm -f "/system/app/Flipboard.odex";
	busybox rm -f "/data/app/flipboard.app-1.apk";
	busybox rm -f "/data/app/flipboard.app-1.odex";
fi;
if [ ! $(check_prop "item.0.2=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/MobilePrintSvc_HP.apk";
	busybox rm -f "/system/app/MobilePrintSvc_HP.odex";
	busybox rm -f "/data/app/com.hp.android.printservice-1.apk";
	busybox rm -f "/data/app/com.hp.android.printservice-1.odex";
fi;
if [ ! $(check_prop "item.0.3=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/IdeaSketch_H.apk";
	busybox rm -f "/system/app/IdeaSketch_H.odex";
fi;
if [ ! $(check_prop "item.0.4=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/SecKidsModeInstaller.apk";
	busybox rm -f "/system/app/SecKidsModeInstaller.odex";
fi;
if [ ! $(check_prop "item.0.5=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/ContentsAgent.apk";
	busybox rm -f "/system/app/ContentsAgent.odex";
	busybox rm -f "/system/app/ContainerAgent.odex";
	busybox rm -f "/system/app/Bridge.apk";
	busybox rm -f "/system/app/Bridge.odex";
	busybox rm -f "/system/app/KNOXAgent.apk";
	busybox rm -f "/system/app/KNOXAgent.odex";
	busybox rm -f "/system/app/ContainerEventsRelayManager.apk";
	busybox rm -f "/system/app/ContainerEventsRelayManager.odex";
	busybox rm -f "/system/app/KLMSAgent.apk";
	busybox rm -f "/system/app/KLMSAgent.odex";
	busybox rm -f "/system/app/KnoxAttestationAgent.apk";
	busybox rm -f "/system/app/KnoxAttestationAgent.odex";
	busybox rm -f "/system/app/KNOXStore.apk";
	busybox rm -f "/system/app/KNOXStore.odex";
	busybox rm -f "/system/app/KNOXStub.apk";
	busybox rm -f "/system/app/KNOXStub.odex";
	busybox rm -f "/system/app/SPDClient.apk";
	busybox rm -f "/system/app/SPDClient.odex";
	busybox rm -f "/system/priv-app/KNOXAgent.apk";
	busybox rm -f "/system/priv-app/KNOXAgent.odex";
	busybox rm -f "/system/priv-app/ContainerEventsRelayManager.apk";
	busybox rm -f "/system/priv-app/ContainerEventsRelayManager.odex";
	busybox rm -f "/system/priv-app/KLMSAgent.apk";
	busybox rm -f "/system/priv-app/KLMSAgent.odex";
	busybox rm -f "/system/priv-app/KnoxAttestationAgent.apk";
	busybox rm -f "/system/priv-app/KnoxAttestationAgent.odex";
	busybox rm -f "/system/priv-app/KNOXStore.apk";
	busybox rm -f "/system/priv-app/KNOXStore.odex";
	busybox rm -f "/system/priv-app/KNOXStub.apk";
	busybox rm -f "/system/priv-app/KNOXStub.odex";
	busybox rm -f "/system/priv-app/SPDClient.apk";
	busybox rm -f "/system/priv-app/SPDClient.odex";
	busybox rm -f "/data/app/com.sec.knox.containeragent";
	busybox rm -f "/data/app/com.sec.knox.eventsmanager";
	busybox rm -f "/data/app/com.sec.enterprise.knox.attestation";
	busybox rm -f "/data/app/com.sec.knox.app.container";
	busybox rm -f "/data/app/com.sec.knox.seandroid";
	busybox rm -f "/data/app/com.sec.knox.store";
	busybox rm -f "/data/app/com.samsung.klmsagent";
	busybox rm -f "/system/tima_measurement_info";
	busybox rm -f "/system/etc/secure_storage/com.sec.knox.store/ss_id";
	busybox rm -f "/system/containers/framework/KNOXModeSwitcher.apk";
	busybox rm -f "/system/containers/framework/KNOXModeSwitcher.odex";
	busybox rm -f "/system/containers/framework/sec_container_1.zzzKnoxLauncher.apk";
	busybox rm -f "/system/containers/framework/sec_container_1.zzzKnoxLauncher.odex";
	busybox rm -f "/system/lib/libknox_encryption.so";
fi;
if [ ! $(check_prop "item.0.6=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/MobilePrint3.apk";
	busybox rm -f "/system/app/MobilePrint3.odex";
fi;
if [ ! $(check_prop "item.0.7=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/MobilePrintSvc_CUPS.apk";
	busybox rm -f "/system/app/MobilePrintSvc_CUPS.odex";
fi;
if [ ! $(check_prop "item.0.8=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/MobilePrintSvc_CUPS_Backend.apk";
	busybox rm -f "/system/app/MobilePrintSvc_CUPS_Backend.odex";
fi;
if [ ! $(check_prop "item.0.9=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/MusicCommonUtility.apk";
	busybox rm -f "/system/app/MusicCommonUtility.odex";
fi;
if [ ! $(check_prop "item.0.10=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/MusicPlayerWT.apk";
	busybox rm -f "/system/app/MusicPlayerWT.odex";
	busybox rm -f "/system/lib/libSamsungMusic.so";
fi;
if [ ! $(check_prop "item.0.11=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/priv-app/MusicFX.apk";
	busybox rm -f "/system/priv-app/MusicFX.odex";
fi;
if [ ! $(check_prop "item.0.12=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SecMyFiles2.apk";
	busybox rm -f "/system/priv-app/SecMyFiles2.odex";
fi;
if [ ! $(check_prop "item.0.13=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/priv-app/MagazineHome.apk";
	busybox rm -f "/system/priv-app/MagazineHome.odex";
fi;
if [ ! $(check_prop "item.0.14=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/Lounge.apk";
	busybox rm -f "/system/app/Lounge.odex";
fi;
if [ ! $(check_prop "item.0.15=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/AllshareMediaServer.apk";
	busybox rm -f "/system/app/AllshareMediaServer.odex";
	busybox rf -f "/system/lib/libasf_mediaserver.so";
	busybox rf -f "/system/lib/libasf_mediashare.so";
fi;
if [ ! $(check_prop "item.0.16=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/AllshareMediaShare.apk";
	busybox rm -f "/system/app/AllshareMediaShare.odex";
fi;
if [ ! $(check_prop "item.0.17=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/priv-app/PageBuddyNotiSvc2.apk";
	busybox rm -f "/system/priv-app/PageBuddyNotiSvc2.odex";
fi;
if [ ! $(check_prop "item.0.18=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/PENUP.apk";
	busybox rm -f "/system/app/PENUP.odex";
	busybox rm -f "/data/app/com.sec.penup-1.apk"
	busybox rm -f "/data/app/com.sec.penup-1.odex";
fi;
if [ ! $(check_prop "item.0.19=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/MediaUploader.apk";
	busybox rm -f "/system/app/MediaUploader.odex";
fi;
if [ ! $(check_prop "item.0.20=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/PolarisOffice5.apk";
	busybox rm -f "/system/app/PolarisOffice5.odex";
	busybox rm -f "/system/lib/libpolarisexternal.lib";
	busybox rm -f "/system/lib/libpolarisexternalSDK.lib";
	busybox rm -f "/system/lib/libpolarisoffice5.lib";
	busybox rm -f "/system/lib/libpolarisofficeSDK.lib";
	busybox rm -f "/system/lib/libpolarisextprint5.lib";
fi;
if [ ! $(check_prop "item.0.21=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/RakeIn.apk";
	busybox rm -f "/system/app/RakeIn.odex";
fi;
if [ ! $(check_prop "item.0.22=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/RoseEUKor.apk";
	busybox rm -f "/system/app/RoseEUKor.odex";
fi;
if [ ! $(check_prop "item.0.23=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/priv-app/GalaxyFinder.apk";
	busybox rm -f "/system/priv-app/GalaxyFinder.odex";
fi;
if [ ! $(check_prop "item.0.24=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SHealth2_5.apk";
	busybox rm -f "/system/priv-app/SHealth2_5.odex";
	busybox rm -f "/system/lib/libhealth_jni.so";
fi;
if [ ! $(check_prop "item.0.25=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SensorService2_5.apk";
	busybox rm -f "/system/priv-app/SensorService2_5.odex";
fi;
if [ ! $(check_prop "item.0.26=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/SNote3.apk";
	busybox rm -f "/system/app/SNote3.odex";
	busybox rm -f "/system/lib/libsnote_core.so";
	busybox rm -f "/system/lib/libSPenGesturePE.so";
	busybox rm -f "/system/lib/libSPenSDKImageFilterLibsPE.so";
fi;
if [ ! $(check_prop "item.0.27=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/SPenSdk3.apk";
	busybox rm -f "/system/app/SPenSdk3.odex";
	busybox rm -f "/data/app/com.samsung.android.sdk.spenv10-1.apk";
	busybox rm -f "/data/app/com.samsung.android.sdk.spenv10-1.odex";
fi;
if [ ! $(check_prop "item.0.28=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/SPlanner_2013.apk";
	busybox rm -f "/system/app/SPlanner_2013.odex";
fi;
if [ ! $(check_prop "item.0.29=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/app/SapaMonitor.apk";
	busybox rm -f "/system/app/SapaMonitor.odex";
fi;
if [ ! $(check_prop "item.0.30=1" $tmppage2) -eq 0 ]; then
	busybox rm -f "/system/priv-app/S_Translator_CSLi_STUB.apk";
	busybox rm -f "/system/priv-app/S_Translator_CSLi_STUB.odex";
	busybox rm -rf "/system/tts/lang_SMT";
	busybox rm -rf "/system/tts/lang_SVOX";
	busybox rm -rf "/system/tts/lang_SVOXP";
	busybox rm -rf "/system/wakeupdata";
	busybox rm -rf "/system/voicebargeindata";
	busybox rm -rf "/system/VODB";
	busybox rm -f "/system/lib/libsamsungtts.so";
fi;
if [ ! $(check_prop "item.0.1=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/Voice_Android_phone.apk";
	busybox rm -f "/system/priv-app/Voice_Android_phone.odex";
fi;
if [ ! $(check_prop "item.0.2=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/Samsungservice_H.apk";
	busybox rm -f "/system/priv-app/Samsungservice_H.odex";
fi;
if [ ! $(check_prop "item.0.3=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SamsungApps_H.apk";
	busybox rm -f "/system/priv-app/SamsungApps_H.odex";
fi;
if [ ! $(check_prop "item.0.4=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/SamsungAppsWidget.apk";
	busybox rm -f "/system/app/SamsungAppsWidget.odex";
fi;
if [ ! $(check_prop "item.0.5=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SamsungBilling.apk";
	busybox rm -f "/system/priv-app/SamsungBilling.odex";
fi;
if [ ! $(check_prop "item.0.6=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/sCloudSyncCalendar.apk";
	busybox rm -f "/system/priv-app/sCloudSyncCalendar.odex";
fi;
if [ ! $(check_prop "item.0.7=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/SamsungContentAgent.apk";
	busybox rm -f "/system/app/SamsungContentAgent.odex";
fi;
if [ ! $(check_prop "item.0.8=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/com.samsung.groupcast-1.apk";
	busybox rm -f "/system/app/com.samsung.groupcast-1.odex";
fi;
if [ ! $(check_prop "item.0.9=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/SamsungHub.apk";
	busybox rm -f "/system/app/SamsungHub.odex";
fi;
if [ ! $(check_prop "item.0.10=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/SamsungHubUpdate_fHD.apk";
	busybox rm -f "/system/app/SamsungHubUpdate_fHD.odex";
fi;
if [ ! $(check_prop "item.0.11=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/SamsungIME.apk";
	busybox rm -f "/system/app/SamsungIME.odex";
fi;
if [ ! $(check_prop "item.0.12=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SamsungLink18.apk";
	busybox rm -f "/system/priv-app/SamsungLink18.odex";
fi;
if [ ! $(check_prop "item.0.13=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/MobilePrintSvc_Samsung.apk";
	busybox rm -f "/system/app/MobilePrintSvc_Samsung.odex";
	busybox rm -f "/data/app/com.sec.app.samsungprintservice-1.apk";
	busybox rm -f "/data/app/com.sec.app.samsungprintservice-1.odex";
fi;
if [ ! $(check_prop "item.0.14=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SPPPushClient_Prod.apk";
	busybox rm -f "/system/priv-app/SPPPushClient_Prod.odex";
	busybox rm -f "/data/app/com.sec.spp.push-1.apk";
	busybox rm -f "/data/app/com.sec.spp.push-1.odex";
fi;
if [ ! $(check_prop "item.0.15=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/sCloudSyncSBrowser.apk";
	busybox rm -f "/system/priv-app/sCloudSyncSBrowser.odex";
fi;
if [ ! $(check_prop "item.0.16=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/SecSetupWizard2013.apk";
	busybox rm -f "/system/app/SecSetupWizard2013.odex";
fi;
if [ ! $(check_prop "item.0.17=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/sCloudSyncSNote3.apk";
	busybox rm -f "/system/priv-app/sCloudSyncSNote3.odex";
fi;
if [ ! $(check_prop "item.0.18=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/sCloudDataSync.apk";
	busybox rm -f "/system/priv-app/sCloudDataSync.odex";
fi;
if [ ! $(check_prop "item.0.19=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SamsungVideo_H.apk";
	busybox rm -f "/system/priv-app/SamsungVideo_H.odex";
fi;
if [ ! $(check_prop "item.0.20=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/Peel.apk";
	busybox rm -f "/system/app/Peel.odex";
	busybox rm -f "/data/app/tv.peel.samsung.app-1.apk";
	busybox rm -f "/data/app/tv.peel.samsung.app-1.odex";
fi;
if [ ! $(check_prop "item.0.21=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/SamsungWatchON.apk";
	busybox rm -f "/system/app/SamsungWatchON.odex";
fi;
if [ ! $(check_prop "item.0.22=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/SamsungSans.apk";
	busybox rm -f "/system/app/SamsungSans.odex";
fi;
if [ ! $(check_prop "item.0.23=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/Pinboard.apk";
	busybox rm -f "/system/priv-app/Pinboard.odex";
fi;
if [ ! $(check_prop "item.0.24=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/MusicLiveShare2.apk";
	busybox rm -f "/system/app/MusicLiveShare2.odex";
fi;
if [ ! $(check_prop "item.0.25=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/ShareVideo.apk";
	busybox rm -f "/system/app/ShareVideo.odex";
fi;
if [ ! $(check_prop "item.0.26=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SketchBookStub.apk";
	busybox rm -f "/system/priv-app/SketchBookStub.odex";
fi;
if [ ! $(check_prop "item.0.27=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/StoryAlbumStubActivationUpgrade.apk";
	busybox rm -f "/system/priv-app/StoryAlbumStubActivationUpgrade.odex";
fi;
if [ ! $(check_prop "item.0.28=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/StoryAlbumWidget.apk";
	busybox rm -f "/system/priv-app/StoryAlbumWidget.odex";
fi;
if [ ! $(check_prop "item.0.29=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/priv-app/Tag.apk";
	busybox rm -f "/system/priv-app/Tag.odex";
fi;
if [ ! $(check_prop "item.0.30=1" $tmppage3) -eq 0 ]; then
	busybox rm -f "/system/app/talkback.apk";
	busybox rm -f "/system/app/talkback.odex";
fi;
if [ ! $(check_prop "item.0.1=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/priv-app/TrimApp_phone_J.apk";
	busybox rm -f "/system/priv-app/TrimApp_phone_J.odex";
fi;
if [ ! $(check_prop "item.0.2=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/priv-app/TripAdvisor_Stub.apk";
	busybox rm -f "/system/priv-app/TripAdvisor_Stub.odex";
fi;
if [ ! $(check_prop "item.0.3=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/app/VideoEditor_Lite_H.apk";
	busybox rm -f "/system/app/VideoEditor_Lite_H.odex";
fi;
if [ ! $(check_prop "item.0.4=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/priv-app/SecVideoPlayerUpg.apk";
	busybox rm -f "/system/priv-app/SecVideoPlayerUpg.odex";
fi;
if [ ! $(check_prop "item.0.5=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/app/VoiceNote4H.apk";
	busybox rm -f "/system/app/VoiceNote4H.odex";
fi;
if [ ! $(check_prop "item.0.6=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/app/VpnClient.apk";
	busybox rm -f "/system/app/VpnClient.odex";
	busybox rm -f "/system/priv-app/VpnDialogs.apk";
	busybox rm -f "/system/priv-app/VpnDialogs.odex";
fi;
if [ ! $(check_prop "item.0.7=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/app/AccuweatherPhone2013.apk";
	busybox rm -f "/system/app/AccuweatherPhone2013.odex";
	
fi;
if [ ! $(check_prop "item.0.8=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/app/MobilePrintSvc_WFDS.apk";
	busybox rm -f "/system/app/MobilePrintSvc_WFDS.odex";
fi;
if [ ! $(check_prop "item.0.9=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/app/AllshareFileShareClient.apk";
	busybox rm -f "/system/app/AllshareFileShareClient.odex";
fi;
if [ ! $(check_prop "item.0.10=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/app/AllshareFileShareServer.apk";
	busybox rm -f "/system/app/AllshareFileShareServer.odex";
fi;
if [ ! $(check_prop "item.0.11=1" $tmppage4) -eq 0 ]; then
	busybox rm -f "/system/app/YahoostockPhone2013.apk";
	busybox rm -f "/system/app/YahoostockPhone2013.odex";
fi;
# ATT
if [ ! $(check_prop "selected.0=1" $tmpvariant) -eq 0 ]; then
	if [ ! $(check_prop "item.0.1=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/APO_ATT.apk";
		busybox rm -f "/system/app/APO_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.2=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/attCodeScanner_vpl_ATT.apk";
		busybox rm -f "/system/app/attCodeScanner_vpl_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.3=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/WISPr_Android22_postpaid_ATT.apk";
		busybox rm -f "/system/app/WISPr_Android22_postpaid_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.4=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/attMessages_vpl_ATT.apk";
		busybox rm -f "/system/app/attMessages_vpl_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.5=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/BeatsMusic_vpl_ATT.apk";
		busybox rm -f "/system/app/BeatsMusic_vpl_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.6=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/priv-app/digitalLocker.apk";
		busybox rm -f "/system/priv-app/digitalLocker.odex";
	fi;
	if [ ! $(check_prop "item.0.7=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/priv-app/DriveMode_ATT.apk";
		busybox rm -f "/system/priv-app/DriveMode_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.8=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/FamilyMap_vpl_ATT.apk";
		busybox rm -f "/system/app/FamilyMap_vpl_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.9=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/priv-app/ISISWallet_ATT.apk";
		busybox rm -f "/system/priv-app/ISISWallet_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.10=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Lookout_ATT.apk";
		busybox rm -f "/system/priv-app/Lookout_ATT.odex";
		busybox rm -f "/system/lib/liblookout.so";
	fi;
	if [ ! $(check_prop "item.0.11=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/myATT_ATT.apk";
		busybox rm -f "/system/app/myATT_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.12=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/priv-app/ready2Go_ATT.apk";
		busybox rm -f "/system/priv-app/ready2Go_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.13=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/skyfireToolbar_ATT.apk";
		busybox rm -f "/system/app/skyfireToolbar_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.14=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/app/SmartWifi_vpl_ATT.apk";
		busybox rm -f "/system/app/SmartWifi_vpl_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.15=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Telenav_ATT.apk";
		busybox rm -f "/system/priv-app/Telenav_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.16=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/priv-app/WildTangent_ATT.apk";
		busybox rm -f "/system/priv-app/WildTangent_ATT.odex";
	fi;
	if [ ! $(check_prop "item.0.17=1" $tmpatt) -eq 0 ]; then
		busybox rm -f "/system/priv-app/YPMobile_ATT.apk";
		busybox rm -f "/system/priv-app/YPMobile_ATT.odex";
	fi;
fi;
#International
if [ ! $(check_prop "selected.0=2" $tmpvariant) -eq 0 ]; then
	if [ ! $(check_prop "item.0.1=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/priv-app/DSMForwarding.apk";
		busybox rm -f "/system/priv-app/DSMForwarding.odex";
	fi;
	if [ ! $(check_prop "item.0.2=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/priv-app/DSMLawmo.apk";
		busybox rm -f "/system/priv-app/DSMLawmo.odex";
	fi;
	if [ ! $(check_prop "item.0.3=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/app/InteractiveTutorial.apk";
		busybox rm -f "/system/app/InteractiveTutorial.odex";
	fi;
	if [ ! $(check_prop "item.0.4=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/app/NfcNci.apk";
		busybox rm -f "/system/app/NfcNci.odex";
	fi;
	if [ ! $(check_prop "item.0.5=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Omc.apk";
		busybox rm -f "/system/priv-app/Omc.odex";
	fi;
	if [ ! $(check_prop "item.0.6=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/app/PickUpTutorial.apk";
		busybox rm -f "/system/app/PickUpTutorial.odex";
	fi;
	if [ ! $(check_prop "item.0.8=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/priv-app/FmmDM.apk";
		busybox rm -f "/system/priv-app/FmmDM.odex";
	fi;
	if [ ! $(check_prop "item.0.9=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/app/PanningTryActually.apk";
		busybox rm -f "/system/app/PanningTryActually.odex";
	fi;
	if [ ! $(check_prop "item.0.10=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/priv-app/SyncmIDS.apk";
		busybox rm -f "/system/priv-app/SyncmIDS.odex";
	fi;
	if [ ! $(check_prop "item.0.11=1" $tmpint) -eq 0 ]; then
		busybox rm -f "/system/app/WebManual.apk";
		busybox rm -f "/system/app/WebManual.odex";
	fi;
fi;
#Sprint
if [ ! $(check_prop "selected.0=3" $tmpvariant) -eq 0 ]; then
	if [ ! $(check_prop "item.0.1=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Chameleon.apk";
		busybox rm -f "/system/priv-app/Chameleon.odex";
	fi;
	if [ ! $(check_prop "item.0.2=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/priv-app/SkyfireToolbar.apk";
		busybox rm -f "/system/priv-app/SkyfireToolbar.odex";
	fi;
	if [ ! $(check_prop "item.0.3=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/priv-app/SprintAndroidExtension.apk";
		busybox rm -f "/system/priv-app/SprintAndroidExtension.odex";
	fi;
	if [ ! $(check_prop "item.0.4=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Sprint_Installer.apk";
		busybox rm -f "/system/priv-app/Sprint_Installer.odex";
	fi;
	if [ ! $(check_prop "item.0.5=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/priv-app/SprintTouch.apk";
		busybox rm -f "/system/priv-app/SprintTouch.odex";
	fi;
	if [ ! $(check_prop "item.0.6=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/app/SprintVoicemail.apk";
		busybox rm -f "/system/app/SprintVoicemail.odex";
	fi;
	if [ ! $(check_prop "item.0.7=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/app/Swype.apk";
		busybox rm -f "/system/app/Swype.odex";
	fi;
	if [ ! $(check_prop "item.0.8=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/priv-app/VoWiFi.apk";
		busybox rm -f "/system/priv-app/VoWiFi.odex";
	fi;
	if [ ! $(check_prop "item.0.9=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/priv-app/VVM.apk";
		busybox rm -f "/system/priv-app/VVM.odex";
	fi;
	if [ ! $(check_prop "item.0.10=1" $tmpspr) -eq 0 ]; then
		busybox rm -f "/system/priv-app/WifiCalling.apk";
		busybox rm -f "/system/priv-app/WifiCalling.odex";
	fi;
fi;
#T-Mobile
if [ ! $(check_prop "selected.0=4" $tmpvariant) -eq 0 ]; then
	if [ ! $(check_prop "item.0.1=1" $tmptmo) -eq 0 ]; then
		busybox rm -f "/system/priv-app/DemoStub_TMO.apk";
		busybox rm -f "/system/priv-app/DemoStub_TMO.odex";
	fi;
	if [ ! $(check_prop "item.0.2=1" $tmptmo) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Lookout_TMO.apk";
		busybox rm -f "/system/priv-app/Lookout_TMO.odex";
		busybox rm -f "/system/lib/liblookout.so";
	fi;
	if [ ! $(check_prop "item.0.3=1" $tmptmo) -eq 0 ]; then
		busybox rm -f "/system/priv-app/AccessTmobile_TMO.apk";
		busybox rm -f "/system/priv-app/AccessTmobile_TMO.odex";
		busybox rm -f "/data/app/com.tmobile.pr.mytmobile-1.apk";
		busybox rm -f "/data/app/com.tmobile.pr.mytmobile-1.odex";
	fi;
	if [ ! $(check_prop "item.0.4=1" $tmptmo) -eq 0 ]; then
		busybox rm -f "/system/priv-app/ECID-release_TMO.apk";
		busybox rm -f "/system/priv-app/ECID-release_TMO.odex";
	fi;
	if [ ! $(check_prop "item.0.5=1" $tmptmo) -eq 0 ]; then
		busybox rm -f "/system/priv-app/com.mobitv.client.tmobiletvhd.apk";
		busybox rm -f "/system/priv-app/com.mobitv.client.tmobiletvhd.odex";
	fi;
	if [ ! $(check_prop "item.0.6=1" $tmptmo) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Vvm_TMO";
		busybox rm -f "/system/priv-app/Vvm_TMO";
		busybox rm -f "/data/app/com.tmobile.vvm.application-1.apk";
		busybox rm -f "/data/app/com.tmobile.vvm.application-1.odex";
	fi;
fi;
#Verizon
if [ ! $(check_prop "selected.0=5" $tmpvariant) -eq 0 ]; then
	if [ ! $(check_prop "item.0.1=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Amazon_Appstore.apk";
		busybox rm -f "/system/priv-app/Amazon_Appstore.odex";
	fi;
	if [ ! $(check_prop "item.0.2=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/Amazon_Audible.apk";
		busybox rm -f "/system/app/Amazon_Audible.odex";
	fi;
	if [ ! $(check_prop "item.0.3=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/Amazon_IMDB.apk";
		busybox rm -f "/system/app/Amazon_IMDB.odex";
	fi;
	if [ ! $(check_prop "item.0.4=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/priv-app/Amazon_MP3.apk";
		busybox rm -f "/system/priv-app/Amazon_MP3.odex";
	fi;
	if [ ! $(check_prop "item.0.5=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/Amazon_Shopping.apk";
		busybox rm -f "/system/app/Amazon_Shopping.odex";
	fi;
	if [ ! $(check_prop "item.0.6=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/Amazon_Widget.apk";
		busybox rm -f "/system/app/Amazon_Widget.odex";
	fi;
	if [ ! $(check_prop "item.0.7=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/Kindle.apk";
		busybox rm -f "/system/app/Kindle.odex";
	fi;
	if [ ! $(check_prop "item.0.8=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/priv-app/LTETest.apk";
		busybox rm -f "/system/priv-app/LTETest.odex";
	fi;
	if [ ! $(check_prop "item.0.9=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/MyInfozone.apk";
		busybox rm -f "/system/app/MyInfozone.odex";
	fi;
	if [ ! $(check_prop "item.0.10=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/priv-app/MyVerizon.apk";
		busybox rm -f "/system/priv-app/MyVerizon.odex";
	fi;
	if [ ! $(check_prop "item.0.11=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/com.gotv.nflgamecenter.us.lite.apk";
		busybox rm -f "/system/app/com.gotv.nflgamecenter.us.lite.odex";
	fi;
	if [ ! $(check_prop "item.0.12=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/Slacker.apk";
		busybox rm -f "/system/app/Slacker.odex";
	fi;
	if [ ! $(check_prop "item.0.13=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/Swype.apk";
		busybox rm -f "/system/app/Swype.odex";
	fi;
	if [ ! $(check_prop "item.0.14=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/VisualVoiceMail.apk";
		busybox rm -f "/system/app/VisualVoiceMail.odex";
	fi;
	if [ ! $(check_prop "item.0.15=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/priv-app/VzCloud.apk";
		busybox rm -f "/system/priv-app/VzCloud.odex";
	fi;
	if [ ! $(check_prop "item.0.16=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/VZNavigator.apk";
		busybox rm -f "/system/app/VZNavigator.odex";
	fi;
	if [ ! $(check_prop "item.0.17=1" $tmpvrz) -eq 0 ]; then
		busybox rm -f "/system/app/VzTones.apk";
		busybox rm -f "/system/app/VzTones.odex";
	fi;
fi;
#Google Apps
if [ ! $(check_prop "item.0.1=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Chrome.apk";
	busybox rm -f "/system/app/Chrome.odex";
	busybox rm -f "/data/app/com.android.chrome-1.apk";
	busybox rm -f "/data/app/com.android.chrome-1.odex";
	busybox rm -f "/data/app/com.android.chrome-2.apk";
	busybox rm -f "/data/app/com.android.chrome-2.odex";
fi;
if [ ! $(check_prop "item.0.2=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Drive.apk";
	busybox rm -f "/system/app/Drive.odex";
	busybox rm -f "/data/app/com.google.android.apps.docs-1.apk";
	busybox rm -f "/data/app/com.google.android.apps.docs-1.odex";
fi;
if [ ! $(check_prop "item.0.3=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/GoogleEarth.apk";
	busybox rm -f "/system/app/GoogleEarth.odex";
	busybox rm -f "/data/app/com.google.earth-1.apk";
	busybox rm -f "/data/app/com.google.earth-1.odex";
	busybox rm -f "/system/lib/libearthmobile.so";
fi;
if [ ! $(check_prop "item.0.4=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/PlusOne.apk";
	busybox rm -f "/system/app/PlusOne.odex";
	busybox rm -f "/data/app/com.google.android.apps.plus-1.apk";
	busybox rm -f "/data/app/com.google.android.apps.plus-1.odex";
fi;
if [ ! $(check_prop "item.0.5=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Gmail2.apk";
	busybox rm -f "/system/app/Gmail2.odex";
	busybox rm -f "/data/app/com.google.android.gm-1.apk";
	busybox rm -f "/data/app/com.google.android.gm-1.odex";
fi;
if [ ! $(check_prop "item.0.6=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Books.apk";
	busybox rm -f "/system/app/Books.odex";
	busybox rm -f "/data/app/com.google.android.apps.books-1.apk";
	busybox rm -f "/data/app/com.google.android.apps.books-1.odex";
fi;
if [ ! $(check_prop "item.0.7=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/PlayGames.apk";
	busybox rm -f "/system/app/PlayGames.odex";
	busybox rm -f "/data/app/com.google.android.play.games-1.apk";
	busybox rm -f "/data/app/com.google.android.play.games-1.odex";
fi;
if [ ! $(check_prop "item.0.8=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Videos.apk";
	busybox rm -f "/system/app/Videos.odex";
	busybox rm -f "/data/app/com.google.android.videos-1.apk";
	busybox rm -f "/data/app/com.google.android.videos-1.odex";
fi;
if [ ! $(check_prop "item.0.9=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Music2.apk";
	busybox rm -f "/system/app/Music2.odex";
	busybox rm -f "/data/app/com.google.android.music-2.apk";
	busybox rm -f "/data/app/com.google.android.music-2.odex";
fi;
if [ ! $(check_prop "item.0.10=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Magazines.apk";
	busybox rm -f "/system/app/Magazines.odex";
	busybox rm -f "/data/app/com.google.android.apps.magazines-1.apk";
	busybox rm -f "/data/app/com.google.android.apps.magazines-1.odex";
fi;
if [ ! $(check_prop "item.0.11=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Velvet.apk";
	busybox rm -f "/system/app/Velvet.odex";
	busybox rm -f "/data/app/com.google.android.googlequicksearchbox-1.apk";
	busybox rm -f "/data/app/com.google.android.googlequicksearchbox-1.odex";
	busybox rm -f "/data/app/com.google.android.googlequicksearchbox-2.apk";
	busybox rm -f "/data/app/com.google.android.googlequicksearchbox-2.odex";
fi;
if [ ! $(check_prop "item.0.12=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/GoogleSearchWidget.apk";
	busybox rm -f "/system/app/GoogleSearchWidget.odex";
fi;
if [ ! $(check_prop "item.0.13=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/Hangouts.apk";
	busybox rm -f "/system/app/Hangouts.odex";
	busybox rm -f "/data/app/com.google.android.talk-1.apk";
	busybox rm -f "/data/app/com.google.android.talk-1.odex";
fi;
if [ ! $(check_prop "item.0.14=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/GMS_Maps.apk";
	busybox rm -f "/system/app/GMS_Maps.odex";
	busybox rm -f "/system/app/Street.apk";
	busybox rm -f "/system/app/Street.odex";
	busybox rm -f "/system/priv-app/Street.apk";
	busybox rm -f "/system/priv-app/Street.odex";
	busybox rm -f "/data/app/com.google.android.maps-1.apk";
	busybox rm -f "/data/app/com.google.android.maps-1.odex";
	busybox rm -f "/data/app/com.google.android.maps-2.apk";
	busybox rm -f "/data/app/com.google.android.maps-2.odex";
fi;
if [ ! $(check_prop "item.0.15=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/data/app/com.google.android.apps.walletnfcrel-1.apk";
	busybox rm -f "/data/app/com.google.android.apps.walletnfcrel-1.odex";
fi;
if [ ! $(check_prop "item.0.16=1" $tmpgoog) -eq 0 ]; then
	busybox rm -f "/system/app/YouTube.apk";
	busybox rm -f "/system/app/YouTube.odex";
	busybox rm -f "/data/app/com.google.android.youtube-1.apk";
	busybox rm -f "/data/app/com.google.android.youtube-1.odex";
	busybox rm -f "/data/app/com.google.android.youtube-2.apk";
	busybox rm -f "/data/app/com.google.android.youtube-2.odex";
fi;

mount -o remount,ro /system;
