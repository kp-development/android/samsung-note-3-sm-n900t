#!/sbin/bash

mount -o remount,rw /;

INITRC="/init.rc";

if grep -q "service sysinit /system/bin/sysinit" "$INITRC"; then
	echo "Init.d Support Exists";
else
	echo "" >> $INITRC;
	echo "service sysinit /system/bin/sysinit" >> $INITRC;
	echo "oneshot" >> $INITRC;
fi;

